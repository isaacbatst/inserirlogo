 <script type="text/javascript" src="http://code.jquery.com/jquery-1.5.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      var i = 0;
       $('#add').on('click', function (){
         if (i < 10)
          $('.inputimg').append('<input type="file" class="my-2" name="imgs[]" id="imgs[]" multiple /><br />');


         i ++;
       });
    });
</script>
<div class="container-fluid bg-light ">
  <div class="container">
    <div class="row  justify-content-center py-5">
      <div class="col-md-8">
        <?php echo $error;?>
        <?=form_open_multipart('upload-do_upload')?>
          <div class="row justify-content-center">
            <div class="col-md-6">
              <h2 class="text-center">Arquivos</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" id="imgcol">
              <div class="form-group ">
                <label class="lead" for="exampleFormControlFile1">Imagem</label>
                <div class="inputimg">
                  <input type="file" id="imgs[]" class="form-control-file my-2" name="imgs[]" size="20" required multiple/>
                </div>
                <input type="button" class="mt-3" id="add" value="+" />

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label  class="lead" for="exampleFormControlFile1">Logo</label>
                <input type="file" name="logo" class="form-control-file my-2" id="exampleFormControlFile1" required/>
              </div>
            </div>
          </div>
          
          
          <h2 class="mb-3">Largura da logo</h2>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="width" placeholder="Tamanho em px" aria-label="Tamanho em px" aria-describedby="basic-addon2" required/>
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2">px</span>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-md-6">
              <h2 class="mb-3 text-center">Detalhes</h2>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-6">
              <h3 class="mb-3 text-center">Posicionamento</h3>
            </div>
            <div class="col-md-6">
              <h3 class="mb-3 text-center">Distância da borda</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-check text-center">
                <input class="form-check-input" type="radio" name="radios" id="exampleRadios1" value="SE" required/>
                <label class="form-check-label" for="exampleRadios1">
                  Superior esquerdo
                </label>
              </div>
              <div class="form-check text-center">
                <input class="form-check-input" type="radio" name="radios" id="exampleRadios2" value="SD">
                <label class="form-check-label" for="exampleRadios2">
                  Superior direito
                </label>
              </div>
              <div class="form-check text-center">
                <input class="form-check-input" type="radio" name="radios" id="exampleRadios2" value="IE">
                <label class="form-check-label" for="exampleRadios2">
                  Inferior esquerdo
                </label>
              </div>
              <div class="form-check text-center">
                <input class="form-check-input" type="radio" name="radios" id="exampleRadios2" value="ID">
                <label class="form-check-label" for="exampleRadios2">
                  Inferior direito
                </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group mb-3">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon1">Horizontal</span>
                </div>
                <input type="text" class="form-control" name="hor" value="50" aria-label="padding-hor" aria-describedby="basic-addon1" required/>
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">px</span>
                </div>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon3">Vertical</span>
                </div>
                <input type="text" class="form-control" name="vrt" value="50" aria-label="paddin-vrt" aria-describedby="basic-addon3" required/>
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon4">px</span>
                </div>
              </div>
            </div>
          </div>

      
          <button type="submit" class="btn btn-primary btn-lg btn-block mt-3">Criar Imagem</button>
        <?=form_close()?>
      </div>

    </div>
    </div>
  </div>
    
  </div>