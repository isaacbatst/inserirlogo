<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MergeController extends CI_Controller {
	public function index(){
			$this->load->view('cabecalho');
			$this->load->view('merge/index');
			$this->load->view('rodape');
		}
	
	public function merging(){
		$config['upload_path'] = './uploads/';
		#$config['allowed_types'] = 'gif|jpg|png';

		$arquivos = $this->input->post();
		$this->load->view('cabecalho');
		$this->load->view('merge/merging',$arquivos);

		$this->load->view('rodape');
	}
}
