<?php 
	class UploadController extends CI_Controller {
		public function __construct() {
			parent::__construct();
		}

		public function index(){
			$this->load->view('cabecalho');
			$this->load->view('upload/index' , array('error' => ''));
			$this->load->view('rodape');
		}


		public function custom_view(){
			$this->load->view('custom_view', array('error' => ' ' ));
		}

		public function multiple(){
			$this->load->view('upload/multiple');
		}

		public function do_upload(){
			$this->load->library('upload');
			$this->load->library('image_lib');

			//posicionamento da logo 
			$posicao = $this->input->post('radios');
			if (!isset($posicao)) {
				echo "Posicionamento da logo não enviado";
				die();
			} else {
				$align = array('vrt' =>'','hor' =>'' );
				if($posicao == "SE"){
					$align['vrt'] = "top";
					$align['hor'] = "left";
					$padding = "50";	
				} else if($posicao == "SD"){
					$align['vrt'] = "top";
					$align['hor'] = "right";
					$padding = "50";
				} else if($posicao == "IE"){
					$align['vrt'] = "bottom";
					$align['hor'] = "left";
					$padding = "0";
				} else if($posicao == "ID"){
					$align['vrt'] = "bottom";
					$align['hor'] = "right";
					$padding = "0";
				}
			}
			
			//criação/verificação caminhos
			$caminho_img = "./uploads/imgs/";
			$caminho_logos = "./uploads/logos/";
			if(!is_dir($caminho_img)){
				mkdir($caminho_img);
			} 
			if (!is_dir($caminho_logos)) {
				mkdir($caminho_logos);
			}

			//config upload logo
			$config = array(
				'upload_path' => $caminho_logos,
				'allowed_types' => "gif|jpg|png|jpeg|pdf",
				'file_name' => md5(date('H:i:s')),
				'file_ext_tolower' => TRUE,
				'overwrite' => TRUE,
			);

			$this->upload->initialize($config);

			//verifica upload da imagem
			if ($this->upload->do_upload('logo')) {
				$logo = $this->upload->data();
			} else {
				echo "Erro: logo não enviada";
				die();
			}

			//tamanho da logo e espaçamento
			$width =  $this->input->post('width');
			$vrt_offset = $this->input->post('vrt');
			$hor_offset = $this->input->post('hor');
			if (!isset($width)|| !isset($vrt_offset)||!isset($hor_offset)) {
				echo "Erro: largura da logo ou distância da borda não enviados";
				die();
			} else{
				$config['source_image'] = $logo['full_path'];
				$config['width'] = $width;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();
			}


			//config upload img
			$config = array(
				'upload_path' => $caminho_img,
				'allowed_types' => "gif|jpg|png|jpeg|pdf",
				#'file_name' => md5(date('H:i:s')),
				'encrypt_name' => TRUE,
				'file_ext_tolower' => TRUE,
				#'overwrite' => TRUE,
			);

			$this->upload->initialize($config);

			//verifica upload da imagem
			

			/*
			foreach ($imgs as $imagem) {
				if ($this->upload->do_upload('imagem')) {
				$imgUp = $this->upload->data();
				} else {
					echo "Erro: imagem não enviada";
					die();
				}
			}
			*/
			$this->load->library('zip');
			//para cada imagem
			$qtd_img = count($_FILES['imgs']['name']);
			for($i = 0; $i < $qtd_img; ++ $i) {
				
				if(!empty($_FILES['imgs']['name'][$i])){
					//montando $imgs
					$_FILES['img']['name'] = $_FILES['imgs']['name'][$i];
					$_FILES['img']['type'] = $_FILES['imgs']['type'][$i];
					$_FILES['img']['tmp_name'] = $_FILES['imgs']['tmp_name'][$i];
					$_FILES['img']['error'] = $_FILES['imgs']['error'][$i];
					$_FILES['img']['size'] = $_FILES['imgs']['size'][$i];

					#echo var_dump($img);
					#echo "<br>";
					#echo var_dump($_FILES['logo']);
					#die();

					if ($this->upload->do_upload('img')) {
					$imgs = $this->upload->data();
					} else {
						echo "Erro: imagem não enviada";
						echo $this->upload->display_errors('<p>', '</p>');
						die();
					}

					$config['source_image'] = $imgs['full_path'];
					$config['wm_overlay_path'] = $logo['full_path'];
					$config['wm_type'] = 'overlay';
					$config['wm_vrt_alignment'] = $align['vrt'];
					$config['wm_hor_alignment'] = $align['hor'];
					$config['wm_vrt_offset'] = $vrt_offset;
					$config['wm_hor_offset'] = $hor_offset;
					$config['wm_opacity'] = '100';
					$config['quality'] = '50';
					$this->image_lib->initialize($config);

					$this->image_lib->watermark();
					//add to zip
					$name='img'.$i.'.jpg';
					$data=$imgs;
					$this->zip->read_file($imgs['full_path']);
					//download
					//force_download($imgs['full_path'], NULL);

					//apagar arquivos
					delete_files($imgs['file_path'],true);
				}
				
				
	        }
	        delete_files($logo['file_path'],true);
			$this->zip->download('fotos_com_logo.zip');

			
			//watermark config
			/*
			$config['source_image'] = $img['full_path'];
			$config['wm_overlay_path'] = $logo['full_path'];
			$config['wm_type'] = 'overlay';
			$config['wm_vrt_alignment'] = $align['vrt'];
			$config['wm_hor_alignment'] = $align['hor'];
			$config['wm_vrt_offset'] = $vrt_offset;
			$config['wm_hor_offset'] = $hor_offset;
			$config['wm_opacity'] = '100';
			*/

			
		}
	}
?>